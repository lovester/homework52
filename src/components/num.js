import React from "react";

const Num = props => {
    return <div className='number'>{props.number}</div>
};

export default Num;