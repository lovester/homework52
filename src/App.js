import React, { Component } from 'react';
import './App.css';
import Num from "./components/num";

class App extends Component {
    state = {
      numbers: []
    };

    createNumbers = () => {
      let numbers = [];
      while (numbers.length < 5) {
        let randomNumber = Math.floor(Math.random() * (36 - 5) + 5);
        if (!numbers.includes(randomNumber)) numbers.push(randomNumber);
      }

      numbers = numbers.sort((a, b) => a - b);

      this.setState({numbers});
    };

    render() {
      return (
          <div>
            <button onClick={this.createNumbers}>New numbers</button>
              {this.state.numbers.map((number, key) => {
                  return <Num key={key} number={number} />;
              })}
          </div>
      )
    }
  }

export default App;


